/*
 * epd_hw.h
 *
 *  Created on: Aug 23, 2019
 *      Author: lorenzo
 */

// Functions for tx/rx to the e-paper display
// Need to change/rewrite these functions if hardware changes
// (e.g. different USCI module/package pins, or different microcontroller)

#ifndef EPD_HW_H_
#define EPD_HW_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"
#include "typedefs.h"
#include "main.h"

// Registers and fields

#define PSR_REG 0x00
#define RES (1 << 6)
#define RES_0 (RES * 0)
#define RES_1 (RES * 1)
#define RES_2 (RES * 2)
#define RES_3 (RES * 3)
#define REG_EN (1 << 5)
#define BWR (1 << 4)
#define UD (1 << 3)
#define SHL (1 << 2)
#define SHD_N (1 << 1)
#define RST_N (1 << 0)

#define PWR_REG 0x01
#define VDS_EN (1 << 1)
#define VDG_EN (1 << 0)
#define VCOM_HW (1 << 2)
#define VGHL_LV (1 << 0)
#define VGHL_LV_0 (VGHL_LV * 0)
#define VGHL_LV_1 (VGHL_LV * 1)
#define VGHL_LV_2 (VGHL_LV * 2)
#define VGHL_LV_3 (VGHL_LV * 3)
#define VDH (1 << 0)
#define VDH_0 (VDH * 0)
#define VDH_1 (VDH * 1)
#define VDH_2 (VDH * 2)
#define VDH_3 (VDH * 3)
#define VDH_4 (VDH * 4)
#define VDH_5 (VDH * 5)
#define VDH_6 (VDH * 6)
#define VDH_7 (VDH * 7)
#define VDL (1 << 0)
#define VDL_0 (VDL * 0)
#define VDL_1 (VDL * 1)
#define VDL_2 (VDL * 2)
#define VDL_3 (VDL * 3)
#define VDL_4 (VDL * 4)
#define VDL_5 (VDL * 5)
#define VDL_6 (VDL * 6)
#define VDL_7 (VDL * 7)
#define VDHR (1 << 0)
#define VDHR_0 (VDHR * 0)
#define VDHR_1 (VDHR * 1)
#define VDHR_2 (VDHR * 2)
#define VDHR_3 (VDHR * 3)
#define VDHR_4 (VDHR * 4)
#define VDHR_5 (VDHR * 5)
#define VDHR_6 (VDHR * 6)
#define VDHR_7 (VDHR * 7)

#define POF_REG 0x02

#define PFS_REG 0x03
#define T_VDS_OFF (1 << 4)
#define T_VDS_OFF_0 (T_VDS_OFF * 0)
#define T_VDS_OFF_1 (T_VDS_OFF * 1)
#define T_VDS_OFF_2 (T_VDS_OFF * 2)
#define T_VDS_OFF_3 (T_VDS_OFF * 3)

#define PON_REG 0x04
#define PMES_REG 0x05

#define BTST_REG 0x06
#define BT_PHA_MASK 0xFF
#define BT_PHB_MASK 0xFF
#define BT_PHC_MASK 0x3F

#define DSLP_REG 0x07
#define DEEP_SLEEP_CHECK (1 << 7 | 1 << 5 | 1 << 2 | 1 << 0)

#define DTM1_REG 0x10  // write OLD data in B/W mode, or B/W data in B/W/R mode

#define DSP_REG 0x11
#define DSP_DATA_FLAG (1 << 7)

#define DRF_REG 0x12
#define DTM2_REG 0x13  // write NEW data in B/W mode, or RED data in B/W/R mode

#define LUTC_REG 0x20
#define LUTWW_REG 0x21
#define LUTBW_REG 0x22
#define LUTR_REG LUTBW
#define LUTWB_REG 0x23
#define LUTW_REG LUTWB
#define LUTBB_REG 0x24
#define LUTB_REG LUTBB

#define PLL_REG 0x30
#define M (1 << 3)
#define M_0 (M * 0)
#define M_1 (M * 1)
#define M_2 (M * 2)
#define M_3 (M * 3)
#define M_4 (M * 4)
#define M_5 (M * 5)
#define M_6 (M * 6)
#define M_7 (M * 7)
#define N (1 << 0)
#define N_0 (N * 0)
#define N_1 (N * 1)
#define N_2 (N * 2)
#define N_3 (N * 3)
#define N_4 (N * 4)
#define N_5 (N * 5)
#define N_6 (N * 6)
#define N_7 (N * 7)

#define TSC_REG 0x40

#define TSE_REG 0x41
#define TSE (1 << 7)
#define TO (1 << 0)
#define TO_0 (TO * 0)
#define TO_1 (TO * 1)
#define TO_2 (TO * 2)
#define TO_3 (TO * 3)
#define TO_4 (TO * 4)
#define TO_5 (TO * 5)
#define TO_6 (TO * 6)
#define TO_7 (TO * 7)

#define TSW_REG 0x42

#define TSR_REG 0x43

#define CDI_REG 0x50
#define VBD (1 << 6)
#define VBD_0 (VBD * 0)
#define VBD_1 (VBD * 1)
#define VBD_2 (VBD * 2)
#define VBD_3 (VBD * 3)
#define DDX (1 << 4)
#define DDX_0 (DDX * 0)
#define DDX_1 (DDX * 1)
#define DDX_2 (DDX * 2)
#define DDX_3 (DDX * 3)
#define CDI (1 << 0)
#define CDI_0 (CDI * 0)
#define CDI_1 (CDI * 1)
#define CDI_2 (CDI * 2)
#define CDI_3 (CDI * 3)
#define CDI_4 (CDI * 4)
#define CDI_5 (CDI * 5)
#define CDI_6 (CDI * 6)
#define CDI_7 (CDI * 7)
#define CDI_8 (CDI * 8)
#define CDI_9 (CDI * 9)
#define CDI_10 (CDI * 10)
#define CDI_11 (CDI * 11)
#define CDI_12 (CDI * 12)
#define CDI_13 (CDI * 13)
#define CDI_14 (CDI * 14)
#define CDI_15 (CDI * 15)

#define LPD_REG 0x51
#define LPD_FLAG (1 << 0)

#define TCON_REG 0x60
#define S2G (1 << 4)
#define S2G_0 (S2G * 0)
#define S2G_1 (S2G * 1)
#define S2G_2 (S2G * 2)
#define S2G_3 (S2G * 3)
#define S2G_4 (S2G * 4)
#define S2G_5 (S2G * 5)
#define S2G_6 (S2G * 6)
#define S2G_7 (S2G * 7)
#define G2S (1 << 0)
#define G2S_0 (G2S * 0)
#define G2S_1 (G2S * 1)
#define G2S_2 (G2S * 2)
#define G2S_3 (G2S * 3)
#define G2S_4 (G2S * 4)
#define G2S_5 (G2S * 5)
#define G2S_6 (G2S * 6)
#define G2S_7 (G2S * 7)

#define TRES_REG 0x61
#define GSST_REG 0x65
#define H_MASK_0 (1 << 0)
#define H_MASK_1 0xF8
#define V_MASK_0 (1 << 0)
#define V_MASK_1 0xFF

#define FLG_REG 0x71
#define PTL_FLAG (1 << 6)
#define I2C_ERR (1 << 5)
#define I2C_BUSY (1 << 4)
#define DATA_FLAG (1 << 3)
#define PON_FLAG (1 << 2)
#define POF_FLAG (1 << 1)
#define BUSY_FLAG (1 << 0)

#define AMV_REG 0x80
#define AMVT (1 << 4)
#define AMVT_0 (AMVT * 0)
#define AMVT_1 (AMVT * 1)
#define AMVT_2 (AMVT * 2)
#define AMVT_3 (AMVT * 3)
#define XON (1 << 3)
#define AMVS (1 << 2)
#define AMV (1 << 1)
#define AMVE (1 << 0)

#define VV_REG 0x81
#define VV_MASK 0x3F

#define VDCS_REG 0x82
#define VDCS_MASK 0x3F

#define PTL_REG 0x90
//#define SET_PARTIAL_WINDOW (1 << 7 | 1 << 4)  // totally fake, datasheet typo
#define PT_SCAN (1 << 0)
// H and V masks declared above
// Reset 3 LSb's in H start, set 3 LSb's in H end

#define PTIN_REG 0x91
#define PTOUT_REG 0x92

#define PGM_REG 0xA0
#define PROG_MODE_CHECK (1 << 7 | 1 << 5 | 1 << 2 | 1 << 0)

#define APG_REG 0xA1
#define ROTP_REG 0xA2

#define PWS_REG 0xE3


// Function prototypes
void epdCS(bool assert);  // true asserts CS (pin low)
void epdRST(bool reset);  // true asserts RST
void epdDC(bool isData);  // true = data, false = command
bool epdBusy();  // true = busy
void epdTxByte(u8 data);

#endif /* EPD_HW_H_ */
