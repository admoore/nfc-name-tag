/**
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/*! \file
 *
 *  \author 
 *
 *  \brief Demo application
 *
 *  This demo shows how to poll for several types of NFC cards/devices and how 
 *  to exchange data with these devices, using the RFAL library.
 *
 *  This demo does not fully implement the activities according to the standards,
 *  it performs the required to communicate with a card/device and retrieve 
 *  its UID. Also blocking methods are used for data exchange which may lead to
 *  long periods of blocking CPU/MCU.
 *  For standard compliant example please refer to the Examples provided
 *  with the RFAL library.
 * 
 */

/*
 ******************************************************************************
 * INCLUDES
 ******************************************************************************
 */
#include <string.h>
#include <stdlib.h>
#include "demo.h"
#include "utils.h"
#include "rfal_rf.h"
#include "rfal_nfcDep.h"
#include "rfal_isoDep.h"
#include "typedefs.h"

/*
 ******************************************************************************
 * GLOBAL DEFINES
 ******************************************************************************
 */

/* Definition of possible states the demo state machine could have */
#define DEMO_ST_FIELD_OFF			        0
#define DEMO_ST_POLL_ACTIVE_TECH      1
#define DEMO_ST_WAIT_WAKEUP	          2

#define DEMO_BUF_LEN                  255

/* macro to cycle through states */
#define	NEXT_STATE()		             {state++; state %= sizeof(stateArray);}

/*
 ******************************************************************************
 * LOCAL VARIABLES
 ******************************************************************************
 */

/* P2P communication data */
static uint8_t NFCID3[] = { 0x01, 0xFE, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x09, 0x0A };
static uint8_t GB[] = { 0x46, 0x66, 0x6d, 0x01, 0x01, 0x11, 0x02, 0x02, 0x07,
		0x80, 0x03, 0x02, 0x00, 0x03, 0x04, 0x01, 0x32, 0x07, 0x01, 0x03 };

/* P2P communication data */
static uint8_t ndefPing[] = {
		0x00, 0x00
				// DSAP = 0x00 (6 bits, well-known, (LLC Link Management component))
				// PTYPE = 0x0 (4 bits, SYMM (symmetry), service class 1/2/3)
				// SSAP = 0x00 (6 bits, well-known, (LLC Link Management component))
};

static uint8_t ndefInit[] = {
		// LLCP layer
		0x05, 0x20,
				// DSAP = 0x01 (6 bits, well-known, SDP)
				// PTYPE = 0x4 (4 bits, CONNECT, service class 2/3)
				// SSAP = 0x20 (6 bits, local servce environment, NOT advertised by local SDP)
		// TLV 0
			0x06,  // Type = 0x06 (SN (Service Name))
			0x0F,  // Length = 0x0F (15 decimal)
			// ASCII: "urn:nfc:sn:snep" (15 chars, no null term)
			0x75, 0x72, 0x6E, 0x3A, 0x6E, 0x66, 0x63, 0x3A, 0x73, 0x6E, 0x3A,
			0x73, 0x6E, 0x65, 0x70,

		// TLV 1
			0x02,  // Type = 0x02 (MIUX (Maximum Information Unit Extension)
			0x02,  // Length = 0x02
			0x07, 0x80,  // 0x780 (1920 decimal)

		// TLV 2
			0x05,  // Type = 0x05 (RW (Receive Window Size)
			0x01,  // Length = 0x01
			0x02  // 0x2
};

static uint8_t ndefUriSTcom[] = {  // 34 bytes
		// LLCP layer
		// LLCP header (3 bytes):
		0x13, 0x20,
				// DSAP = 0x04 (well-known, see ASREG (Assigned Numbers Registry))
				// PTYPE = 0xC (I (information), service class 2/3)
				// SSAP = 0x20 (local servce environment, NOT advertised by local SDP)
		0x00,
				// N(S) = 0x0 (sequence number for this I PDU)
				// N(R) = 0x0 (sequence number of last I PDU received + 1)

		// LLCP information field (31 bytes):
			// SNEP layer
			// SNEP header (6 bytes):
			0x10,  // SNEP version (1.0)
			0x02,  // SNEP request: Put
			0x00, 0x00, 0x00, 0x19,  // Length of information field (decimal 25)

			// SNEP information field:
				// NDEF message
				// NDEF header (7 bytes):
				// Begin actual NDEF message (first 2 bytes not in txbuf):
				// 0xD1 (TNF + flags, binary 1101_0001; begin, end, short record, type 1
				// 0x01 (Type length)
				0xc1,  // TNF + flags = 1100_0001 binary (message begin, message end, TNF well-known)
				0x01,  // Type length
				0x00, 0x00, 0x00, 0x12,  // Payload length (decimal 18)
				// No ID length
				0x55,// Payload type (ASCII U)
				// No payload ID

				// NDEF payload (18 bytes):
					0x00,  // URI Record Type Definition (none/as-written)
					// URI: "http://www.ti.com"
					0x68, 0x74, 0x74, 0x70, 0x3a, 0x2f, 0x2f, 0x77, 0x77, 0x77,
					0x2e, 0x74, 0x69, 0x2e, 0x63, 0x6f, 0x6d
};

/*
 ******************************************************************************
 * LOCAL VARIABLES
 ******************************************************************************
 */

/*! Transmit buffers union, only one interface is used at a time                                                            */
static union {
	rfalIsoDepApduBufFormat isoDepTxBuf; /* ISO-DEP Tx buffer format (with header/prologue) */
	rfalNfcDepBufFormat nfcDepTxBuf; /* NFC-DEP Rx buffer format (with header/prologue) */
	uint8_t txBuf[DEMO_BUF_LEN]; /* Generic buffer abstraction                      */
} gTxBuf;

/*! Receive buffers union, only one interface is used at a time                                                             */
static union {
	rfalIsoDepApduBufFormat isoDepRxBuf; /* ISO-DEP Rx buffer format (with header/prologue) */
	rfalNfcDepBufFormat nfcDepRxBuf; /* NFC-DEP Rx buffer format (with header/prologue) */
	uint8_t rxBuf[DEMO_BUF_LEN]; /* Generic buffer abstraction                      */
} gRxBuf;

/*! Receive buffers union, only one interface is used at a time                                                             */
static union {
	rfalIsoDepDevice isoDepDev; /* ISO-DEP Device details                          */
	rfalNfcDepDevice nfcDepDev; /* NFC-DEP Device details                          */
} gDevProto;

/*
 ******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 ******************************************************************************
 */

static bool demoPollAP2P(void);
static ReturnCode demoActivateP2P(uint8_t* nfcid, uint8_t nfidLen,
		bool isActive, rfalNfcDepDevice *nfcDepDev);
static ReturnCode demoNfcDepBlockingTxRx(rfalNfcDepDevice *nfcDepDev,
		const uint8_t *txBuf, uint16_t txBufSize, uint8_t *rxBuf,
		uint16_t rxBufSize, uint16_t *rxActLen);
static void demoSendNdefUri(void);

// Max length of tx is less than 255 bytes (for some reason)
//const char *URL = "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef";
//#define PAYLOAD_STR "Hello World!"  // can find strlen at compile time
static bool init = true;
static uint8_t *txData;
static uint8_t txDataSize;

void demoCycle(void) {
	if (init) {
		u8 payloadType = 'T';
		const char *payloadStr = "0123456789abcdef"
				"0123456789abcdef"
				"0123456789abcdef";
		// This is the limit (max size), txDataSize = 251 (decimal)
//		const char *payloadStr =
//				"0123456789abcdef"
//				"0123456789abcdef"
//				"0123456789abcdef"
//				"0123456789abcdef"
//
//				"0123456789abcdef"
//				"0123456789abcdef"
//				"0123456789abcdef"
//				"0123456789abcdef"
//
//				"0123456789abcdef"
//				"0123456789abcdef"
//				"0123456789abcdef"
//				"0123456789abcdef"
//
//				"0123456789abcdef"
//				"0123456789abcdef"
//				"0123456789abc";
		u8 initialByte = 0x00;

		u8 LLCP_DSAP = 0x04 & 0x3F;
		u8 LLCP_PTYPE = 0xC & 0xF;
		u8 LLCP_SSAP = 0x20 & 0x3F;
		u8 LLCP_NS = 0 & 0xF;
		u8 LLCP_NR = 0 & 0xF;

		u8 SNEP_MajorVersion = 1 & 0xF;
		u8 SNEP_MinorVersion = 0 & 0xF;
		u8 SNEP_request = 0x02;
		u32 SNEP_infoLength;

		bool NDEF_messageStart = true;
		bool NDEF_messageEnd = true;
		bool NDEF_chunk = false;
		bool NDEF_shortRecord;
		bool NDEF_idLengthPresent = false;
		u8 NDEF_TNF = 0x1 & 0x7;
		u8 NDEF_typeLength = 1;
		u32 NDEF_payloadSize = 1 + strlen(payloadStr);
		u8 NDEF_idLength = 0;
		u8 *NDEF_payloadType = malloc(NDEF_typeLength);
		u8 *NDEF_id = malloc(NDEF_idLength);
		*NDEF_payloadType = payloadType;
		NDEF_shortRecord = (NDEF_payloadSize < 0xFF) ? true : false;

		u8 LLCP_headerSize = 3;
		u8 SNEP_headerSize = 6;
		u8 NDEF_headerSize = 2 +
				(NDEF_shortRecord ? 1 : 4) +
				(NDEF_idLengthPresent ? 1 : 0) +
				NDEF_typeLength + NDEF_idLength;
		SNEP_infoLength = NDEF_headerSize + NDEF_payloadSize;

		txDataSize = LLCP_headerSize + SNEP_headerSize + NDEF_headerSize +
				NDEF_payloadSize;
		txData = malloc(txDataSize);

		u32 i = 0;

		// LLCP
		*(txData + i++) = (LLCP_DSAP << 2) | (LLCP_PTYPE >> 2);
		*(txData + i++) = (LLCP_PTYPE << 6) | (LLCP_SSAP);
		*(txData + i++) = (LLCP_NS << 4) | (LLCP_NR);

		// SNEP
		*(txData + i++) = (SNEP_MajorVersion << 4) | (SNEP_MinorVersion);
		*(txData + i++) = SNEP_request;
		*(txData + i++) = (SNEP_infoLength >> 24) & 0xFF;
		*(txData + i++) = (SNEP_infoLength >> 16) & 0xFF;
		*(txData + i++) = (SNEP_infoLength >>  8) & 0xFF;
		*(txData + i++) = (SNEP_infoLength >>  0) & 0xFF;

		// NDEF
		*(txData + i++) = (NDEF_messageStart ? 1 << 7 : 0) |
				(NDEF_messageEnd ? 1 << 6 : 0) |
				(NDEF_chunk ? 1 << 5 : 0) |
				(NDEF_shortRecord ? 1 << 4 : 0) |
				(NDEF_idLengthPresent ? 1 << 3 : 0) |
				NDEF_TNF;
		*(txData + i++) = NDEF_typeLength;
		if (NDEF_shortRecord) {
			*(txData + i++) = NDEF_payloadSize;
		} else {
			*(txData + i++) = (NDEF_payloadSize >> 24) & 0xFF;
			*(txData + i++) = (NDEF_payloadSize >> 16) & 0xFF;
			*(txData + i++) = (NDEF_payloadSize >>  8) & 0xFF;
			*(txData + i++) = (NDEF_payloadSize >>  0) & 0xFF;
		}
		if (NDEF_idLengthPresent) {
			*(txData + i++) = NDEF_idLength;
		}
		memmove(txData + i, NDEF_payloadType, NDEF_typeLength);
		i += NDEF_typeLength;
		memmove(txData + i, NDEF_id, NDEF_idLength);
		i += NDEF_idLength;
		*(txData + i++) = initialByte;
		memmove(txData + i, payloadStr, strlen(payloadStr));

		init = false;
	}
	demoPollAP2P();
	HAL_Delay(500);
}

/*!
 *****************************************************************************
 * \brief Poll NFC-AP2P
 *
 * Configures the RFAL to AP2P communication and polls for a nearby 
 * device. If a device is found turns On a LED and logs its UID.
 * If the Device supports NFC-DEP protocol (P2P) it will activate 
 * the device and try to send an URI record.
 *
 * This methid first tries to establish communication at 424kb/s and if
 * failed, tries also at 106kb/s
 * 
 * 
 *  \return true    : AP2P device found
 *  \return false   : No device found
 * 
 *****************************************************************************
 */
bool demoPollAP2P(void) {
	ReturnCode err;
	bool try106 = false;

	while (!try106) {
		/*******************************************************************************/
		/* NFC_ACTIVE_POLL_MODE                                                        */
		/*******************************************************************************/
		/* Initialize RFAL as AP2P Initiator NFC BR 424 */
		err = rfalSetMode(RFAL_MODE_POLL_ACTIVE_P2P,
				((try106) ? RFAL_BR_106 : RFAL_BR_424),
				((try106) ? RFAL_BR_106 : RFAL_BR_424));

		rfalSetErrorHandling(RFAL_ERRORHANDLING_NFC);
		rfalSetFDTListen(RFAL_FDT_LISTEN_AP2P_POLLER);
		rfalSetFDTPoll(RFAL_TIMING_NONE);

		rfalSetGT(RFAL_GT_AP2P_ADJUSTED);
		err = rfalFieldOnAndStartGT();

		err = demoActivateP2P(NFCID3, RFAL_NFCDEP_NFCID3_LEN, true,
				&gDevProto.nfcDepDev);
		if (err == ERR_NONE) {
			/****************************************************************************/
			/* Active P2P device activated                                              */
			/* NFCID / UID is contained in : nfcDepDev.activation.Target.ATR_RES.NFCID3 */
			platformLog("NFC Active P2P device found. NFCID3: %s\r\n",
					hex2Str(gDevProto.nfcDepDev.activation.Target.ATR_RES.NFCID3, RFAL_NFCDEP_NFCID3_LEN));
			platformLedOn(PLATFORM_LED_AP2P_PORT, PLATFORM_LED_AP2P_PIN);

			/* Send an URI record */
			demoSendNdefUri();
			return true;
		}

		/* AP2P at 424kb/s didn't found any device, try at 106kb/s */
		try106 = true;
		rfalFieldOff();
	}

	return false;
}

/*!
 *****************************************************************************
 * \brief Activate P2P
 *
 * Configures NFC-DEP layer and executes the NFC-DEP/P2P activation (ATR_REQ 
 * and PSL_REQ if applicable)
 *  
 * \param[in] nfcid      : nfcid to be used
 * \param[in] nfcidLen   : length of nfcid
 * \param[in] isActive   : Active or Passive communiccation
 * \param[out] nfcDepDev : If activation successful, device's Info
 * 
 *  \return ERR_PARAM    : Invalid parameters
 *  \return ERR_TIMEOUT  : Timeout error
 *  \return ERR_FRAMING  : Framing error detected
 *  \return ERR_PROTO    : Protocol error detected
 *  \return ERR_NONE     : No error, activation successful
 * 
 *****************************************************************************
 */
ReturnCode demoActivateP2P(uint8_t* nfcid, uint8_t nfidLen, bool isActive,
		rfalNfcDepDevice *nfcDepDev) {
	rfalNfcDepAtrParam nfcDepParams;

	nfcDepParams.nfcid = nfcid;
	nfcDepParams.nfcidLen = nfidLen;
	nfcDepParams.BS = RFAL_NFCDEP_Bx_NO_HIGH_BR;
	nfcDepParams.BR = RFAL_NFCDEP_Bx_NO_HIGH_BR;
	nfcDepParams.LR = RFAL_NFCDEP_LR_254;
	nfcDepParams.DID = RFAL_NFCDEP_DID_NO;
	nfcDepParams.NAD = RFAL_NFCDEP_NAD_NO;
	nfcDepParams.GBLen = sizeof(GB);
	nfcDepParams.GB = GB;
	nfcDepParams.commMode = (
			(isActive) ? RFAL_NFCDEP_COMM_ACTIVE : RFAL_NFCDEP_COMM_PASSIVE);
	nfcDepParams.operParam = (RFAL_NFCDEP_OPER_FULL_MI_EN
			| RFAL_NFCDEP_OPER_EMPTY_DEP_DIS | RFAL_NFCDEP_OPER_ATN_EN
			| RFAL_NFCDEP_OPER_RTOX_REQ_EN);

	/* Initialize NFC-DEP protocol layer */
	rfalNfcDepInitialize();

	/* Handle NFC-DEP Activation (ATR_REQ and PSL_REQ if applicable) */
	return rfalNfcDepInitiatorHandleActivation(&nfcDepParams, RFAL_BR_424,
			nfcDepDev);
}

/*!
 *****************************************************************************
 * \brief Send URI
 *
 * Sends a NDEF URI record 'http://www.ST.com' via NFC-DEP (P2P) protocol.
 * 
 * This method sends a set of static predefined frames which tries to establish
 * a LLCP connection, followed by the NDEF record, and then keeps sending 
 * LLCP SYMM packets to maintain the connection.
 *  
 * 
 *  \return true    : NDEF URI was sent
 *  \return false   : Exchange failed
 * 
 *****************************************************************************
 */
void demoSendNdefUri(void) {
	uint16_t actLen = 0;
	ReturnCode err = ERR_NONE;

	platformLog(" Initalize device .. ");
	if (ERR_NONE
			!= demoNfcDepBlockingTxRx(&gDevProto.nfcDepDev, ndefInit,
					sizeof(ndefInit), gRxBuf.rxBuf, sizeof(gRxBuf.rxBuf),
					&actLen)) {
		platformLog("failed.");
		return;
	}
	platformLog("succeeded.\r\n");

	actLen = 0;
	platformLog(" Push NDEF Uri: www.ST.com .. ");
	if (ERR_NONE
			!= demoNfcDepBlockingTxRx(&gDevProto.nfcDepDev, txData,
					txDataSize, gRxBuf.rxBuf, sizeof(gRxBuf.rxBuf), &actLen)) {
		platformLog("failed.");
		return;
	}

//	platformLog(" Device present, maintaining connection ");
//	while (err == ERR_NONE) {
//		err = demoNfcDepBlockingTxRx(&gDevProto.nfcDepDev, ndefPing,
//				sizeof(ndefPing), gRxBuf.rxBuf, sizeof(gRxBuf.rxBuf), &actLen);
//		platformLog(".");
//		platformDelay(50);
//	}
	platformLog("\r\n Device removed.\r\n");
}

/*!
 *****************************************************************************
 * \brief NFC-DEP Blocking Transceive 
 *
 * Helper function to send data in a blocking manner via the rfalNfcDep module 
 *  
 * \warning A protocol transceive handles long timeouts (several seconds), 
 * transmission errors and retransmissions which may lead to a long period of 
 * time where the MCU/CPU is blocked in this method.
 * This is a demo implementation, for a non-blocking usage example please 
 * refer to the Examples available with RFAL
 *
 * \param[in]  nfcDepDev  : device details retrived during activation
 * \param[in]  txBuf      : data to be transmitted
 * \param[in]  txBufSize  : size of the data to be transmited
 * \param[out] rxBuf      : buffer to place receive data
 * \param[in]  rxBufSize  : size of the reception buffer
 * \param[out] rxActLen   : number of data bytes received

 * 
 *  \return ERR_PARAM     : Invalid parameters
 *  \return ERR_TIMEOUT   : Timeout error
 *  \return ERR_FRAMING   : Framing error detected
 *  \return ERR_PROTO     : Protocol error detected
 *  \return ERR_NONE      : No error, activation successful
 * 
 *****************************************************************************
 */
ReturnCode demoNfcDepBlockingTxRx(rfalNfcDepDevice *nfcDepDev,
		const uint8_t *txBuf, uint16_t txBufSize, uint8_t *rxBuf,
		uint16_t rxBufSize, uint16_t *rxActLen) {
	ReturnCode err;
	bool isChaining;
	rfalNfcDepTxRxParam rfalNfcDepTxRx;

	/* Initialize the NFC-DEP protocol transceive context */
	rfalNfcDepTxRx.txBuf = &gTxBuf.nfcDepTxBuf;
	rfalNfcDepTxRx.txBufLen = txBufSize;
	rfalNfcDepTxRx.rxBuf = &gRxBuf.nfcDepRxBuf;
	rfalNfcDepTxRx.rxLen = rxActLen;
	rfalNfcDepTxRx.DID = RFAL_NFCDEP_DID_NO;
	rfalNfcDepTxRx.FSx = rfalNfcDepLR2FS(
			rfalNfcDepPP2LR( nfcDepDev->activation.Target.ATR_RES.PPt ));
	rfalNfcDepTxRx.FWT = nfcDepDev->info.FWT;
	rfalNfcDepTxRx.dFWT = nfcDepDev->info.dFWT;
	rfalNfcDepTxRx.isRxChaining = &isChaining;
	rfalNfcDepTxRx.isTxChaining = false;

	/* Copy data to send */
	ST_MEMMOVE(gTxBuf.nfcDepTxBuf.inf, txBuf,
			MIN(txBufSize, RFAL_NFCDEP_FRAME_SIZE_MAX_LEN));

	/* Perform the NFC-DEP Transceive in a blocking way */
	rfalNfcDepStartTransceive(&rfalNfcDepTxRx);
	do {
		rfalWorker();
		err = rfalNfcDepGetTransceiveStatus();
	} while (err == ERR_BUSY);

	/* platformLog(" NFC-DEP TxRx %s: - Tx: %s Rx: %s \r\n", (err != ERR_NONE) ? "FAIL": "OK", hex2Str( (uint8_t*)rfalNfcDepTxRx.txBuf, txBufSize), (err != ERR_NONE) ? "": hex2Str( rfalNfcDepTxRx.rxBuf->inf, *rxActLen)); */

	if (err != ERR_NONE) {
		return err;
	}

	/* Copy received data */
	ST_MEMMOVE(rxBuf, gRxBuf.nfcDepRxBuf.inf, MIN(*rxActLen, rxBufSize));
	return ERR_NONE;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
