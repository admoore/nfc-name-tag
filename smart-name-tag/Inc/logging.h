/*
 * logging.h
 *
 *  Created on: Jan 17, 2020
 *      Author: admoore
 */

#ifndef INC_LOGGING_H_
#define INC_LOGGING_H_

#include "main.h"
#include "logger.h"  // ST logging util
#include <stdio.h>

#define ERROR 1
#define WARNING 2
#define INFO 3
#define TRACE 4
#define LOGGER_LEVEL INFO

#define LOG(level, args...) if(LOGGER_LEVEL >= level){ char str[256] = ""; \
							uint8_t offset = printLevel(str, level); \
							offset += sprintf(str + offset, args); \
							offset += sprintf(str + offset, "\r\n"); \
							transmit(str); }

uint8_t printLevel(char* str, uint8_t level);

void transmit(char* str);

extern UART_HandleTypeDef huart3;

#endif /* INC_LOGGING_H_ */
