#!/bin/bash

## partialGen.sh
## Lorenzo Castoldi 2020
## Create two images for use on a monochrome e-paper display (EPD) (simple bin format)
## More complex to generate than a single monolithic framebuffer, but saves
##     memory and enables partial refreshes
## Also creates success/fail images, exported as C header/source to bake into firmware

# Configure as necessary
MAX_X=400
MAX_Y=300
NAME_FONT_SIZE=72
SCHOOL_FONT_SIZE=84
MESSAGE_FONT_SIZE=38
NAME_FONT="MyriadPro-Semibold"
SCHOOL_FONT="MinionPro-Semibold"
MESSAGE_FONT="MyriadPro-Semibold"
NAME="Jane Doe"
SCHOOL="Atwater U"

# Make some subdirs to keep things organized
mkdir -p pbm bin

# Create images (PBM format: 1 bit per pixel, but with header and unknown resolution)
convert -size ${MAX_X}x -depth 1 -background white -fill black -pointsize $NAME_FONT_SIZE   -font $NAME_FONT   caption:"$NAME"   -trim +repage -bordercolor white -border 1 pbm/topText.pbm
convert -size ${MAX_X}x -depth 1 -background white -fill black -pointsize $SCHOOL_FONT_SIZE -font $SCHOOL_FONT caption:"$SCHOOL" -trim +repage -bordercolor white -border 1 pbm/bottomText.pbm

# Get resolutions
TOP_X=$(identify pbm/topText.pbm | sed -rn 's/[^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\1/p')
TOP_Y=$(identify pbm/topText.pbm | sed -rn 's/[^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\2/p')
BOTTOM_X=$(identify pbm/bottomText.pbm | sed -rn 's/[^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\1/p')
BOTTOM_Y=$(identify pbm/bottomText.pbm | sed -rn 's/[^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\2/p')

# X resolution must be a multiple of 8, round up if needed
TOP_X=$(( ($TOP_X + 7) / 8 * 8 ))
BOTTOM_X=$(( ($BOTTOM_X + 7) / 8 * 8 ))

# Check for fit on screen
if [ $TOP_X -gt $MAX_X ] || [ $BOTTOM_X -gt $MAX_X ] || [ $(( $TOP_Y + $BOTTOM_Y )) -gt $MAX_Y ]; then
    echo "ERROR: Text does not fit within screen resolution. Shrink font size or use shorter strings"
    exit 1
fi

# Regenerate images with proper resolution
convert pbm/topText.pbm    -depth 1 -background black -gravity northwest -negate -extent ${TOP_X}x${TOP_Y}       pbm/topText.pbm
convert pbm/bottomText.pbm -depth 1 -background black -gravity southwest -negate -extent ${BOTTOM_X}x${BOTTOM_Y} pbm/bottomText.pbm

# Find size to extract (rest of file is header)
TOP_BYTES=$(( ($TOP_X * $TOP_Y) / 8 ))
BOTTOM_BYTES=$(( ($BOTTOM_X * $BOTTOM_Y) / 8 ))

# Extract important bytes, skip header
tail --bytes $TOP_BYTES pbm/topText.pbm > bin/topText.bin
tail --bytes $BOTTOM_BYTES pbm/bottomText.pbm > bin/bottomText.bin

# Convert bin files to C header (array of bytes)
# xxd -i bin/topText.bin > images.c
# xxd -i bin/bottomText.bin >> images.c

# Add "const" keyword to all variables, so images stay in flash (never in memory)
#sed -ri 's/^unsigned/const unsigned/' images.c

# Remove unnecessary "bin" from variable names
#sed -ri 's/bin_(.*)_bin/\1/' images.c

# Add x/y resolutions for each image
printf 'const unsigned int topText_w = %d;\n' $TOP_X >> images.c
printf 'const unsigned int topText_h = %d;\n' $TOP_Y >> images.c
printf 'const unsigned int bottomText_w = %d;\n' $BOTTOM_X >> images.c
printf 'const unsigned int bottomText_h = %d;\n' $BOTTOM_Y >> images.c


## Success/failure messages
#     convert -depth 1 -background white -fill black -negate -pointsize $NUMBER_FONT_SIZE -font $NUMBER_FONT caption:"Success!" pbm/succ.pbm
#     convert -depth 1 -background white -fill black -negate -pointsize $NUMBER_FONT_SIZE -font $NUMBER_FONT caption:"Failure" pbm/fail.pbm
convert -size ${MAX_X}x -depth 1 -background white -fill black -pointsize $MESSAGE_FONT_SIZE -font $MESSAGE_FONT caption:"Success!" -trim +repage -bordercolor white -border 1 pbm/succ.pbm
convert -size ${MAX_X}x -depth 1 -background white -fill black -pointsize $MESSAGE_FONT_SIZE -font $MESSAGE_FONT caption:"Failure" -trim +repage -bordercolor white -border 1 pbm/fail.pbm

# Get resolutions
SUCC_X=$(identify pbm/succ.pbm | sed -rn 's/[^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\1/p')
SUCC_Y=$(identify pbm/succ.pbm | sed -rn 's/[^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\2/p')
FAIL_X=$(identify pbm/fail.pbm | sed -rn 's/[^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\1/p')
FAIL_Y=$(identify pbm/fail.pbm | sed -rn 's/[^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\2/p')

# X resolution must be a multiple of 8, round up if needed
SUCC_X=$(( ($SUCC_X + 7) / 8 * 8 ))
FAIL_X=$(( ($FAIL_X + 7) / 8 * 8 ))

# Regenerate images with proper resolution
# convert -size ${SUCC_X}x${SUCC_Y} -depth 1 -background white -fill black -negate -pointsize $NUMBER_FONT_SIZE -font $NUMBER_FONT caption:"Success!" pbm/succ.pbm
# convert -size ${FAIL_X}x${FAIL_Y} -depth 1 -background white -fill black -negate -pointsize $NUMBER_FONT_SIZE -font $NUMBER_FONT caption:"Failure" pbm/fail.pbm
convert pbm/succ.pbm -depth 1 -background black -gravity southeast -negate -extent ${SUCC_X}x${SUCC_Y} pbm/succ.pbm
convert pbm/fail.pbm -depth 1 -background black -gravity southeast -negate -extent ${FAIL_X}x${FAIL_Y} pbm/fail.pbm

# Find size to extract (rest of file is header)
SUCC_BYTES=$(( ($SUCC_X * $SUCC_Y) / 8 ))
FAIL_BYTES=$(( ($FAIL_X * $FAIL_Y) / 8 ))

# Extract important bytes, skip header
tail --bytes $SUCC_BYTES pbm/succ.pbm > bin/succ.bin
tail --bytes $FAIL_BYTES pbm/fail.pbm > bin/fail.bin

# Convert bin files to C header (array of bytes)
xxd -i bin/succ.bin >  images.c
xxd -i bin/fail.bin >> images.c

# Add "const" keyword to all variables, so images stay in flash (never in memory)
sed -ri 's/^unsigned/const unsigned/' images.c

# Remove unnecessary "bin" from variable names
sed -ri 's/bin_(.*)_bin/\1/' images.c

# Add x/y resolutions for each image
printf 'const unsigned int succ_w = %d;\n' $SUCC_X >> images.c
printf 'const unsigned int succ_h = %d;\n' $SUCC_Y >> images.c
printf 'const unsigned int fail_w = %d;\n' $FAIL_X >> images.c
printf 'const unsigned int fail_h = %d;\n' $FAIL_Y >> images.c



## Numbers
## Numbers 0-9 for counter, all same size

# Generate digits 0-9
# for NUM in {0..9}; do
#     convert -depth 1 -background white -fill black -negate -pointsize $NUMBER_FONT_SIZE -font $NUMBER_FONT caption:$NUM pbm/${NUM}.pbm
# done

# Get size and verify all same size (and size valid)
# Hacky regex because of digit in filename
# NUM_X=$(identify pbm/0.pbm | sed -rn 's/[^[:digit:]]*[[:digit:]][^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\1/p')
# NUM_Y=$(identify pbm/0.pbm | sed -rn 's/[^[:digit:]]*[[:digit:]][^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\2/p')
# 
# if [ $(( $NUM_X % 8 )) -ne 0 ]; then
#     echo "ERROR: digit width not a multiple of 8px"
#     exit 1
# fi

# for NUM in {1..9}; do
#     if [ $NUM_X -ne $(identify pbm/${NUM}.pbm | sed -rn 's/[^[:digit:]]*[[:digit:]][^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\1/p') ] ||
#        [ $NUM_Y -ne $(identify pbm/${NUM}.pbm | sed -rn 's/[^[:digit:]]*[[:digit:]][^[:digit:]]*([[:digit:]]+)x([[:digit:]]+).*/\2/p') ]; then
#         printf "Digit %d size mismatch\r\n" $NUM
#         exit 1
#     fi
# done

# Same routine as before
# NUM_BYTES=$((($NUM_X * $NUM_Y) / 8))
# for NUM in {0..9}; do
#     tail --bytes $NUM_BYTES pbm/${NUM}.pbm > bin/${NUM}.bin
#     xxd -i bin/${NUM}.bin >> images.c
# done
# sed -ri 's/^unsigned/const unsigned/' images.c
# sed -ri 's/bin_(.*)_bin/digit\1/' images.c  # prefix with `digit' (e.g. digit0, digit1, ...)
# sed -ri 's/^.*digit[[:digit:]]_len.*$//' images.c  # remove redundant `digitX_len' lines
# echo 'const unsigned char *digits[] = {digit0, digit1, digit2, digit3, digit4,
# 		digit5, digit6, digit7, digit8, digit9};' >> images.c
# printf 'const unsigned int digit_len = %d;\n' $NUM_BYTES >> images.c
# printf 'const unsigned int digit_w = %d;\n' $NUM_X >> images.c
# printf 'const unsigned int digit_h = %d;\n' $NUM_Y >> images.c


## Create header file (fixes multiple inclusion)
echo "#ifndef IMAGES_H_" > ../../Inc/image-gen/images.h
echo "#define IMAGES_H_" >> ../../Inc/image-gen/images.h 
cat images.c >> ../../Inc/image-gen/images.h
perl -i -0pe 's/\s=[^;]*;/;/g' ../../Inc/image-gen/images.h 
sed -ri 's/^const/extern const/' ../../Inc/image-gen/images.h 
echo "#endif" >> ../../Inc/image-gen/images.h


# Done!
echo "Success!"
printf "Top: %dx%d\n" $TOP_X $TOP_Y
printf "Bottom: %dx%d\n" $BOTTOM_X $BOTTOM_Y
# printf "Digits: %dx%d\n" $NUM_X $NUM_Y
printf "Success: %dx%d\n" $SUCC_X $SUCC_Y
printf "Failure: %dx%d\n" $FAIL_X $FAIL_Y
exit 0
