/*
 * epd.h
 *
 *  Created on: Aug 23, 2019
 *      Author: lorenzo
 */

// "Driver"-style functions to control e-paper display
// No porting needed, just rewrite SPI tx/rx functions

#ifndef EPD_H_
#define EPD_H_

#include "typedefs.h"
#include "epd_hw.h"

#define EPD_WIDTH 400
#define EPD_WIDTH_BYTES 50
#define EPD_HEIGHT 300

void epdInit();
void epdClear();
void epdImage(const u8 *blackImage, const u8 *redImage);
void epdPattern(u8 blackPattern, u8 redPattern);
void epdSleep();

#endif /* EPD_H_ */
