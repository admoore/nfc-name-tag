#ifndef IMAGES_H_
#define IMAGES_H_
extern const unsigned char succ[];
extern const unsigned int succ_len;
extern const unsigned char fail[];
extern const unsigned int fail_len;
extern const unsigned int succ_w;
extern const unsigned int succ_h;
extern const unsigned int fail_w;
extern const unsigned int fail_h;
#endif
