/*
 * fsOps.h
 *
 *  Created on: Mar 1, 2020
 *      Author: lorenzo
 */

#ifndef FSOPS_H_
#define FSOPS_H_

#include <stdio.h>
#include "typedefs.h"
#include "nfc.h"

u8 createFS();  // format (if needed), mount, create base files/folders
u8 loadImages(u8 **topIm, u8 **botIm, int *topW, int *topH, int *botW, int *botH);
u8 loadVCF(u8 **buf, uint *size);  // read VCF from flash, load into buffer
ReturnCode writeVcf();  // read from VCF rx buffer, write to flash

#endif /* FSOPS_H_ */
