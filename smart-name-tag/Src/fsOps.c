/*
 * fsOps.c
 *
 *  Created on: Mar 1, 2020
 *      Author: lorenzo
 */

#include <stdio.h>
#include "main.h"
#include "fatfs.h"
#include "typedefs.h"
#include "st_errno.h"
#include "nfc.h"
#include "storage.h"
#include "logging.h"

u8 createFS() {
	// Create/mount filesystem
	uint8_t res = f_mount(&USERFatFS, USERPath, 1);
	do {  // no loop, just so we can use break for errors
		if (res != FR_OK) {
			LOG(ERROR, "Failed to mount file system. Creating one.");
			uint8_t *buf = (uint8_t*) calloc(8 * SECTOR_SIZE, sizeof(uint8_t)); // Allocate 32KB of memory for the working buffer.
			res = f_mkfs(USERPath, FM_FAT, BLOCK_SIZE, buf, 8 * SECTOR_SIZE);
			if (res != FR_OK) {
				LOG(ERROR, "Failed to create file system, error %d", res);
				break;
			}
			free(buf);
			res = f_mount(&USERFatFS, USERPath, 1);
			if (res != FR_OK) {
				LOG(ERROR, "Failed to mount created file system, error %d", res);
				break;
			}
		} else {
			LOG(INFO, "Successfully mounted file system.");
		}

		// Change back/create starter files
		// (Make sure we're in root dir)
		res = f_chdir("/");
		if (res != FR_OK) {
			LOG(ERROR, "Could not cd into /, error %d", res);
			break;
		}

		// In /; create README.txt (overwrite existing contents, if any)
		res = f_open(&USERFile, "README.TXT", FA_CREATE_ALWAYS | FA_WRITE);
		if (res == FR_OK) {
			uint bytes_written;
			char *str =
					"How to use the Smart Name Tag:\r\n\r\n"
							"1. Create your own VCF file with your contact information.\r\n"
							"       You can export this from your smartphone's address book,\r\n"
							"       or create one from scratch at http://bvcard.com/vCard-vcf-generator\r\n\r\n"
							"2. Rename the file to `MY_INFO.VCF` (without the quotes),\r\n"
							"       then put it here in the root folder of the name tag.\r\n\r\n"
							"3. Create images for your name tag. You can have a top image (your name),\r\n"
							"       and a bottom image (your organization). The images must be in a simple,\r\n"
							"       header-less `.BIN` format; 1 bit per pixel, 8 pixels per byte.\r\n"
							"       Put these files in the `IMAGES` folder, named `TOP_IM.BIN` and `BOT_IM.BIN,\r\n"
							"       and fill out the 'RES.TXT' file with their resolutions.\r\n"
							"		The width of all images must be a multiple of 8.\r\n"
							"4. Network! To exchange contact information, simply hold two name tags\r\n"
							"       together until the LED lights up solid for a second or so.\r\n"
							"       The screen will display `Success` if all has gone well.\r\n\r\n"
							"5. To access your received contacts, look in the `contacts` folder.\r\n"
							"       You can easily import these with your phone, or an email client such as Outlook.\r\n";
			f_write(&USERFile, str, strlen(str), &bytes_written);
			f_close(&USERFile);
		} else {
			platformLog("Could not open README.TXT for writing, error %d\r\n",
					res);
			break;
		}

		// In /; create LICENSE.txt (overwrite existing contents, if any)
		res = f_open(&USERFile, "LICENSE.TXT", FA_CREATE_ALWAYS | FA_WRITE);
		if (res == FR_OK) {
			uint bytes_written;
			char *str =
					"This device uses third-party software, licensed as follows:\r\n\r\n"
							"- The HAL library is licensed by ST under the BSD 3-Clause License,\r\n"
							"      available at opensource.org/licenses/BSD-3-Clause\r\n"
							"      Copyright 2017 STMicroelectronics\r\n"
							"- The ST25R391x firmware is licensed by ST under the ST MYLIBERTY SOFTWARE LICENSE AGREEMENT,\r\n"
							"      available at http://www.st.com/myliberty\r\n"
							"      Copyright 2016 STMicroelectronics\r\n"
							"- The USB MSC firmware is licensed by ST under the Ultimate Liberty license,\r\n"
							"		available at www.st.com/SLA0044\r\n"
							"		Copyright 2015 STMicroelectronics\r\n"
							"- The FatFs open source software\r\n"
							"       Copyright 2017 ChaN\r\n";
			f_write(&USERFile, str, strlen(str), &bytes_written);
			f_close(&USERFile);
		} else {
			platformLog("Could not open LICENSE.TXT for writing, error %d\r\n",
					res);
			break;
		}

		// Make and/or cd into /IMAGES
		res = f_chdir("CONTACTS");
		if (res != FR_OK) {
			LOG(ERROR,
					"Could not change directories, error %d\r\nAttempting to create...",
					res);
			res = f_mkdir("CONTACTS");
			if (res != FR_OK) {
				LOG(ERROR, "Could not create /CONTACTS dir, error %d", res);
				break;
			}

			f_chdir("CONTACTS");
			if (res != FR_OK) {
				LOG(ERROR, "Could not cd into /CONTACTS, error %d", res);
				break;
			}
		}

		// cd back to /
		res = f_chdir("/");
		if (res != FR_OK) {
			LOG(ERROR, "Could not cd into /. Error code %d.", res);
			break;
		}

		// Make and/or cd into /IMAGES
		res = f_chdir("IMAGES");
		if (res != FR_OK) {
			LOG(ERROR,
					"Could not change directories (error %d). Attempting to create.",
					res);
			res = f_mkdir("IMAGES");

			if (res != FR_OK) {
				LOG(ERROR, "Could not create folder, error %d", res);
				break;
			}

			f_chdir("IMAGES");
			if (res != FR_OK) {
				LOG(ERROR, "Could not change into new directory, error %d", res);
				break;
			}
		}

		// In /IMAGES; create resolution file template if not present
		res = f_open(&USERFile, "RES.TXT", FA_OPEN_EXISTING);  // check for file
		if (res != FR_OK) {  // not present--create with temp values
			platformLog("RES.TXT does not exist--creating\r\n");
			res = f_open(&USERFile, "RES.TXT", FA_CREATE_ALWAYS | FA_WRITE);
			if (res == FR_OK) {
				uint bytes_written;
				char *str = "TOP_W=1\r\nTOP_H=2\r\nBOT_W=3\r\nBOT_H=4\r\n";
				f_write(&USERFile, str, strlen(str), &bytes_written);
				f_close(&USERFile);
			} else {
				platformLog("Could not open RES.TXT for writing\r\n");
				break;
			}
		} else {
			platformLog("RES.TXT exists, not modified\r\n");
			f_close(&USERFile);
		}

		res = f_chdir("/");
		if (res != FR_OK) {
			LOG(ERROR, "Could not change directories. Error code %d.", res);
			break;
		}
	} while (false);

	return res;
}

// Free images in epdDispName unless an error occurs, then free here
u8 loadImages(u8 **topIm, u8 **botIm, int *topW, int *topH, int *botW,
		int *botH) {
	FRESULT res;
	FILINFO inf;

	// cd into /IMAGES
	res = f_chdir("IMAGES");
	if (res != FR_OK) {
		LOG(ERROR, "Could not cd to /IMAGES, error %d", res);
		return res;
	}

	// Read resolutions from RES.TXT into output args
	res = f_stat("RES.TXT", &inf);
	if (res != FR_OK) {
		LOG(ERROR, "Could not stat RES.TXT, error %d", res);
		return res;
	}
	char *resStr = malloc(inf.fsize);

	res = f_open(&USERFile, "RES.TXT", FA_READ);
	if (res != FR_OK) {
		platformLog("Could not open RES.TXT for reading, error %d\r\n", res);
		free(resStr);
		return res;
	}

	res = f_read(&USERFile, resStr, inf.fsize, NULL);
	if (res != FR_OK) {
		platformLog("Could not read RES.TXT, error %d\r\n", res);
		free(resStr);
		return res;
	}

	res = f_close(&USERFile);
	if (res != FR_OK) {
		platformLog("Could not close RES.TXT, error %d\r\n", res);
		free(resStr);
		return res;
	}

	// scanf--what's the worst that could happen?
	sscanf(resStr, "TOP_W=%d TOP_H=%d BOT_W=%d BOT_H=%d", topW, topH, botW,
			botH);
	free(resStr);

	// Read top image, check filesize against resolutions from file
	res = f_stat("TOP_IM.BIN", &inf);
	if (res != FR_OK) {
		LOG(ERROR, "Could not stat TOP_IM.BIN, error %d", res);
		return res;
	}

	if (inf.fsize != (*topW / 8) * *topH) {
		platformLog("Top image dimensions don't match filesize\r\n");
		return -1;
	}

	res = f_open(&USERFile, "TOP_IM.BIN", FA_READ);
	if (res != FR_OK) {
		platformLog("Could not open TOP_IM.BIN for reading, error %d\r\n", res);
		return res;
	}

	*topIm = malloc(inf.fsize);
	res = f_read(&USERFile, *topIm, inf.fsize, NULL);
	if (res != FR_OK) {
		platformLog("Could not read TOP_IM.BIN, error %d\r\n", res);
		free(*topIm);
		return res;
	}

	res = f_close(&USERFile);
	if (res != FR_OK) {
		platformLog("Could not close TOP_IM.BIN, error %d\r\n", res);
		free(*topIm);
		return res;
	}

	// Read bottom image, check filesize against resolutions from file
	res = f_stat("BOT_IM.BIN", &inf);
	if (res != FR_OK) {
		LOG(ERROR, "Could not stat BOT_IM.BIN, error %d", res);
		free(*topIm);
		return res;
	}

	if (inf.fsize != (*botW / 8) * *botH) {
		platformLog("Bottom image dimensions don't match filesize\r\n");
		free(*topIm);
		return -1;
	}

	res = f_open(&USERFile, "BOT_IM.BIN", FA_READ);
	if (res != FR_OK) {
		platformLog("Could not open BOT_IM.BIN for reading, error %d\r\n", res);
		free(*topIm);
		return res;
	}

	*botIm = malloc(inf.fsize);
	res = f_read(&USERFile, *botIm, inf.fsize, NULL);
	if (res != FR_OK) {
		platformLog("Could not read BOT_IM.BIN, error %d\r\n", res);
		free(*topIm);
		free(*botIm);
		return res;
	}

	res = f_close(&USERFile);
	if (res != FR_OK) {
		platformLog("Could not close BOT_IM.BIN, error %d\r\n", res);
		free(*topIm);
		free(*botIm);
		return res;
	}

	// cd back to /
	res = f_chdir("/");
	if (res != FR_OK) {
		LOG(ERROR, "Could not cd to /, error %d", res);
		free(*topIm);
		free(*botIm);
		return res;
	}

	return res;
}

u8 loadVCF(u8 **buf, uint *size) {
	FRESULT res;
	FILINFO inf;

	res = f_stat("MY_INFO.VCF", &inf);
	if (res != FR_OK) {
		LOG(ERROR, "Could not stat MY_INFO.VCF, error %d", res);
		return res;
	}

	res = f_open(&USERFile, "MY_INFO.VCF", FA_READ);
	if (res != FR_OK) {
		platformLog("Could not open MY_INFO.VCF for reading, error %d\r\n",
				res);
		return res;
	}

	*buf = malloc(inf.fsize);  // free in makeVcfTxBuf()
	res = f_read(&USERFile, *buf, inf.fsize, size);
	if (res != FR_OK) {
		platformLog("Could not read MY_INFO.VCF, error %d\r\n", res);
		return res;
	}

	res = f_close(&USERFile);
	if (res != FR_OK) {
		platformLog("Could not close MY_INFO.VCF, error %d\r\n", res);
		return res;
	}

	return res;
}

ReturnCode writeVcf() {
	u8 res;
	ReturnCode ret = ERR_NONE;
	u16 vcfIndex = 0;  // suffix for VCF files on FS (e.g. 23 for CON00023.VCF)
	char filename[13];  // 13 chars, enough for 8.3 plus null
	do {  // no loop, just break on errors

		// cd to /CONTACTS
		res = f_chdir("CONTACTS");
		if (res != FR_OK) {
			LOG(ERROR, "Could not cd to /CONTACTS, error %d", res);
			ret = ERR_FILESYSTEM;
			break;
		}

		// Find the first file that doesn't exist
		do {
			snprintf(filename, 13, "CON%05d.VCF", vcfIndex);
			res = f_stat(filename, NULL);
			if (res == FR_OK) {
				if (vcfIndex == 0xFFFFFFFF) {
					platformLog(
							"All 4 billion files already exist somehow\r\n");
					ret = ERR_FILESYSTEM;
					break;
				} else {
					vcfIndex++;
				}
			}
		} while (res != FR_NO_FILE);
		platformLog("Write to %s\r\n", filename);

		// In /CONTACTS; create CONxxxxx.VCF (overwrite existing contents, if any [shouldn't be any])
		res = f_open(&USERFile, filename, FA_CREATE_ALWAYS | FA_WRITE);
		if (res == FR_OK) {
			uint bytes_written;
			// TODO should error-check all write/close ops, but especially these
			f_write(&USERFile, VcfRxBuffer, VcfRxSize, &bytes_written);
			f_close(&USERFile);
		} else {
			platformLog("Could not open %s for writing\r\n", filename);
			ret = ERR_FILESYSTEM;
			break;
		}

		// cd back to /
		res = f_chdir("/");
		if (res != FR_OK) {
			LOG(ERROR, "Could not cd to /, error %d", res);
			ret = ERR_FILESYSTEM;
			break;
		}
	} while (false);
	return ret;
}
