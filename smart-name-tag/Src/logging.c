/*
 * logging.c
 *
 *  Created on: Jan 17, 2020
 *      Author: admoore
 */

#include "logging.h"

uint8_t printLevel(char *str, uint8_t level) {
	switch (level) {
	case 1:
		sprintf(str, "ERROR: ");
		return 7;
	case 2:
		sprintf(str, "WARNING: ");
		return 9;
	case 3:
		sprintf(str, "INFO: ");
		return 6;
	case 4:
		sprintf(str, "TRACE: ");
		return 7;
	default:
		return 0;
	}
}

void transmit(char *str) {
	platformLog(str);  // just use ST log function
	//HAL_UART_Transmit(&huart3, (uint8_t*) str, 256, 5000);
}
